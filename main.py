﻿# pyhebcal - A Hebrew Calendar system implemented in Python
# Copyright (C) 2010  Tsvi Mostovicz -- ttmost@gmail.com

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

## These files implements the Jewish clendar according to the specification mentioned in the Tur Shu"a
## The functions have been tested in comparison to the test vectors / homework provided
## by Shay Walter of KBY

## TODO:
##  - Implement function to define chasera, kesidra, or shlema
##  - Rewrite test vectors in a formal way
##  - Create a function to convert gregorian to hebrew date
##     * This can be done by anchoring a known date
##     * Conversion is being done by measuring amount of days from anchor date both for gd and jd
##  - Add test units for all functions (especially overloaded operators)
##  - Store type of dchiya implemented for this year
##  - Deprecate test in this file, use unittest

##  http://cgate.co.il/calendar%5Ccalendar.htm

from hebcalsys import jdate

for temp in [5718,  5719, 5766,  5678,  5708,  5710,  5738,  5735,  5688,  5745,  5715,  5746, 5753]:
#for temp in range(5760,5770):
    today = jdate.jyear(temp)
    temp += 1
    print(today.year, today._molad_tishri.show(), end=' ')
    if today._isleap == True:
        print("Meuberet")
    else:
        print("Pshuta")
    print("Year has " + str(today._year_length) + " days", end=' ')
    print("Rosh hashana will be on the %s day of the week" % (today._rh_dow))
