﻿# jdate.py - A class defining the Hebrew Date in Python
# This class is part of pyhebcal
# Copyright (C) 2010  Tsvi Mostovicz -- ttmost@gmail.com

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# TODO: - Change molad into a tuple or a list object ?
#         Depends on testing

class molad:
    '''Defines a molad object.
    Molad is a time object used in the Hebrew calendar system to calculate the new moon.
    It is defined as a certain amount of days, hours and chalakim (literally pieces).
    One day has 24 hours, 1 hour = 1080 chalakim'''
    def __init__(self, dow, hour, chalakim):
        '''Initialization function'''
        self.dow = dow
        self.hour = hour
        self.chalakim = chalakim

    def show(self):
        '''Print the molad nicely'''
        print("Molad at " + str(self.dow) + " days, " + str(self.hour) + " hours and " + str(self.chalakim) + " chalakim", end=' ')
        return 0

    def __add__(self, other):
        '''Overload of addition for molad object'''
        temp = self
        temp.chalakim = self.chalakim + other.chalakim
        temp.hour = self.hour + other.hour + temp.chalakim // 1080
        temp.dow  = self.dow + other.dow + temp.hour // 24
        temp.chalakim = temp.chalakim % 1080
        temp.hour = temp.hour % 24
        return temp

    def __mul__(self, other):
        '''Overload of multiplication for molad object.
        Only works for multplying a molad object by an int'''
        if type(other) is int:
            temp = self
            temp.chalakim = self.chalakim * other
            temp.hour = self.hour * other + temp.chalakim // 1080
            temp.dow = self.dow * other + temp.hour // 24
            temp.chalakim = temp.chalakim % 1080
            temp.hour = temp.hour % 24
            return temp
        else:
            return NotImplemented

    def __le__(self, other):
        '''Overload less or equal for molad objects'''
        if self.hour < other.hour:
            return True
        elif self.hour == other.hour:
            if self.chalakim <= other.chalakim:
                return True
        else:
            return False

    def __ge__(self, other):
        '''Overload greater or equal for molad objects'''
        if self.hour > other.hour:
            return True
        elif self.hour == other.hour:
            if self.chalakim >= other.chalakim:
                return True
        else:
            return False

    def __lt__(self, other):
        '''Overload less then for molad objects'''
        if self.hour < other.hour:
            return True
        elif self.hour == other.hour:
            if self.chalakim < other.chalakim:
                return True
        else:
            return False

    def __gt__(self, other):
        '''Overload greater then for molad objects'''
        if self.hour > other.hour:
            return True
        elif self.hour == other.hour:
            if self.chalakim > other.chalakim:
                return True
        else:
            return False

    def __eq__(self,other):
        '''Overload equal operator for molad object'''
        if self.chalakim == other.chalakim and self.hour == other.hour and self.dow == other.dow:
            return True
        else:
            return False

class jyear:
    '''A class defining the Hebrew year.
    Given a Hebrew year (eg. 5771), all characteristics will be returned:
        - Being it leap or not
        - Day of week for Rosh Hashana
        - Year length in days (Which defines length of Cheshvan and Kislev)'''
    leapyear = [0, 3, 6, 8, 11, 14, 17]

    def __init__(self,  year):
        '''Initialization function'''
        self.year = year
        self._isleap = self.isleap()
        self._molad_tishri = self.molad_tishri() # type is molad
        self._rh_dow = self.rh_dow(self.year,  self._molad_tishri)
        self._year_length = self.year_length()

    def isleap(self):
        '''Checks to see that the year is a leapyear.
        This is defined according to the גו"ח אדז"ט rule'''
        if self.year % 19 in jyear.leapyear:
            return True
        else:
            return False

    def molad_tishri(self):
        '''Return molad time of the molad of Tishri of requested year
        Defined by time since molad בהר"ד of creation'''
        creation = molad(2, 5, 204)   # molad baharad
        next_year_p = molad(4, 8, 876)
        next_year_m = molad(5, 21, 589)
        next_cycle = molad(2, 16, 595)
        meuberet_until_current = 0

        for i  in range(1, self.year % 19):
            if i in jyear.leapyear:
                meuberet_until_current += 1

        if self.year % 19 == 0:
            molad_total = creation + (next_cycle * ((self.year // 19)-1))
            molad_total += next_year_m * 6
            molad_total += next_year_p * 12
            molad_total.dow = molad_total.dow % 7
            return molad_total

        molad_total = creation + (next_cycle * (self.year // 19))
        molad_total += next_year_m * meuberet_until_current
        molad_total += next_year_p * (self.year % 19 - meuberet_until_current - 1)
        molad_total.dow = molad_total.dow % 7
        return molad_total

    def rh_dow(self,  year,  this_molad):
        '''Return day of week on which Rosh Hashana of this year will fall
        This is done by comparing molad tishri of this year to the different dchiyot:
          - Get red (ג"ט ר"ד)
          - Betu Takpat (ב' ט"ו תקפ"ט)
          - Molad Zaken (מולד זקן)
          - Lo adu rosh (לא אד"ו ראש)'''
        rh = this_molad.dow
        molad_18hr = molad(0, 18, 0)
        molad_9_204 = molad(0, 9, 204)
        molad_15_589 = molad(0, 15, 589)
        if rh == 3 and molad_18hr > this_molad >= molad_9_204 and ((year % 19) not in jyear.leapyear):
            rh = 5
#            print "get red %s" % (rh)
        elif rh == 2 and molad_18hr > this_molad >= molad_15_589 and (((year - 1) % 19) in jyear.leapyear):
            rh = 3
#            print "betu takpat %s" % (rh)
        elif this_molad >= molad_18hr and rh in [0, 2, 3, 5]:
            rh += 1
#            print "Molad Zaken %s" % (rh)
        if rh in [1, 4, 6]:
            rh +=1
#            print "Lo adu rosh %s" % (rh)
        return rh

    def year_length(self):
        '''Returns the years length.
        The years length is dependent on the day of week of Rosh Hashana of this year, compared
        to the day of week of next year.'''
        if self._isleap == True:
            next_molad = self._molad_tishri + molad(5, 21, 589)
        else:
            next_molad = self._molad_tishri + molad(4, 8, 876)
        next_molad.dow = next_molad.dow % 7
        diff = self.rh_dow(self.year+1,  next_molad) - self._rh_dow
        if diff <= 0:
            diff += 7
        if self._isleap == True:
            return diff + 378
        else:
            return diff + 350
