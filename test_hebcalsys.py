﻿# test_hebcalsys.py - Unit tests for the hebcalsys package
# This class is part of pyhebcal
# Copyright (C) 2010  Tsvi Mostovicz -- ttmost@gmail.com

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# TODO: - Move this file to correct location within tests
#       - Fix assertEqual for molad_tishri (different types)

import unittest
from hebcalsys import jdate

class KnownValues(unittest.TestCase):
    '''Defines known values for given years.
    Values are defined as: (year, isleap, year_length, rh_dow, (molad_tishri))'''
    known_values = ( (5718,False,354,5,jdate.molad(0,21,510)),
                     (5719,True,383,2,jdate.molad(6,19,19)),
                     (5766,False,354,3,jdate.molad(0,1,672)),
                     (5678,False,355,2,jdate.molad(6,6,15)),
                     (5708,True,385,2,jdate.molad(1,15,171)),
                     (5710,False,353,7,jdate.molad(3,8,843)),
                     (5738,True,384,3,jdate.molad(2,11,614)),
                     (5735,False,354,3,jdate.molad(0,7,720)),
                     (5688,False,354,3,jdate.molad(0,1,67)),
                     (5745,False,354,5,jdate.molad(1,2,772)),
                     (5715,False,354,3,jdate.molad(0,6,329)),
                     (5746,True,383,2,jdate.molad(0,0,281)),
                     (5753,False,353,2,jdate.molad(5,15,439)) )

    def test_is_leap_known_values(self):
        '''is_leap should give known result with known input'''
        for year, is_leap, year_length,rh_dow, molad_tishri in self.known_values:
            result = jdate.jyear(year)
            self.assertEqual(is_leap, result._isleap)
            self.assertEqual(year_length, result._year_length)
            self.assertEqual(rh_dow, result._rh_dow)
            self.assertEqual(molad_tishri, result._molad_tishri)

if __name__ == '__main__':
    unittest.main()

